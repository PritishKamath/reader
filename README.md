# OCR Writer- Write Data to BigQuery after OCR

#### Endpoints

  Contains 2 api endpoints
  
| Operation | Usage | Method | Description |
| ------ | ------ | ------ | ------ |
| Insert | http://host:port/insert/n | GET | n can be any number from 1 to 5. Inserts the record of nth position|
| Truncate | http://host:port/truncate/table_name | DELETE | Deletes all records in the table. Truncate of a table is possible only after 90 minutes of last insert |
  
#### Building the jar

  - Clone the repository
  - Run the following command
     ```sh
        mvn clean install
     ```
  - The jar would be created in the ./target folder

#### Configurations

The following configurations are possible

| Key | Description | Default |
| ------ | ------ | ------ |
| google.bigquery.datasetName | The destination dataset | ocr_demo_dataset |
| google.bigquery.tableName | The table to write data to | ocr_demo_table |
| google.bigquery.projectId | The project id in Google Cloud Platform | q-ai-sales |
| google.service.client.file | The service client file for accessing BigQuery table. To specify a custom one, entire file path has to be provided | q-ai-sales-new.json |
| server.port | The port at which the application runs | 8080 |

The configurations must be specified in a standard properties file, to be used at runtime as given in the 'Run Application' section

#### Run Application

   If there are no changes in the configuration, the following command is to be used for running the application 
  ```
  cd ./target
  java -jar reader-1.0.jar
  ```
  
  To specify a custom configuration file, use the following command
  
  ```
  java -jar reader-1.0.jar --spring.config.location=classpath:/path/to/file.properties
  ```
