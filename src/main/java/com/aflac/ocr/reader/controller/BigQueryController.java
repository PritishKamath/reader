package com.aflac.ocr.reader.controller;


import com.aflac.ocr.reader.framework.BigQueryComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class BigQueryController {
    @Autowired
    BigQueryComponent bigQueryComponent;

    @RequestMapping(value = "/insert/{id}", method = RequestMethod.GET)
    public String insertBigQuery(@PathVariable("id") String id){
        String message = bigQueryComponent.insertToBQ(id)  ? "Inserted successfully" :  "Error occured";
        return message;
    }

    @RequestMapping(value = "/truncate/{table_name}", method = RequestMethod.DELETE)
    public String truncateBigQuery(@PathVariable("table_name") String tableName){
        String message = bigQueryComponent.truncate(tableName) ? "Deleted successfully" :  "Error occured";
        return message;
    }
}
