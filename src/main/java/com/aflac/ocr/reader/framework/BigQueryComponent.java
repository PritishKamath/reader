package com.aflac.ocr.reader.framework;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class BigQueryComponent {

    @Autowired
    private Environment environment;

    public boolean insertToBQ(String id){
        String datasetName = environment.getProperty("google.bigquery.datasetName");
        String tableName = environment.getProperty("google.bigquery.tableName");
        String projectId = environment.getProperty("google.bigquery.projectId");
        String fileName = environment.getProperty("google.service.client.file");
        String dataPoint = "data.json.record" + id;

        String keys = environment.getProperty(dataPoint);
        Map<String, Object> row = ToMap.jsonToMap(new JSONObject(keys));

        BigQuery bigquery = null;
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource(fileName).getFile());
            bigquery = BigQueryOptions.newBuilder().setProjectId(projectId).setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream(file))).build().getService();
            TableId tableId = TableId.of(datasetName, tableName);
            InsertAllRequest insertAllRequest = InsertAllRequest.newBuilder(tableId).addRow(row).build();
            InsertAllResponse insertAllResponse = bigquery.insertAll(insertAllRequest);

            if (insertAllResponse.hasErrors()){
                System.out.println(insertAllResponse.getErrorsFor(0));
                return false;
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean truncate(String tableName){
        String datasetName = environment.getProperty("google.bigquery.datasetName");
        String tableName1 = environment.getProperty("google.bigquery.tableName");
        String projectId = environment.getProperty("google.bigquery.projectId");
        String fileName = environment.getProperty("google.service.client.file");
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());
        BigQuery bigquery = null;
        try {
            bigquery = BigQueryOptions.newBuilder().setProjectId(projectId).setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream(file))).build().getService();
            String query = "DELETE FROM `" + projectId + "." + datasetName + "." + tableName + "` WHERE 1=1;";
            QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(query).build();
            try {
                for (FieldValueList row : bigquery.query(queryConfig).iterateAll()) {

                }
            } catch (InterruptedException e) {
                System.out.println(e);
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }



}
